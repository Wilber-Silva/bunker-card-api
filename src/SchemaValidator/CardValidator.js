const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)

const date = new Date()
const minYear = date.getFullYear()

const ownerId = Joi.objectId().required()
const ownerName = Joi.string()
const ownerDocumentNumber = Joi.string()

const number = Joi.string().regex(/^[0-9]+$/).min(16).max(16)
const brandName = Joi.string()
const month = Joi.number().integer().min(1).max(12)
const year = Joi.number().integer().min(minYear) 

module.exports.create = Joi.object({
    ownerId
    , ownerName
    , ownerDocumentNumber
    , card: Joi.object({
        number
        , brandName
        , month
        , year
    })
})
