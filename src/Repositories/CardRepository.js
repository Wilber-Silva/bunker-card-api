const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { CardSchema } = require('./../Models/card')


class Repository extends BaseRepository {
    constructor() {
        super('cards', CardSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new Repository()