const jwt = require('jwt-simple')
const { ResponseCreated, ResponseError, UnprocessableEntityException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const { create: createSchema } = require('./../SchemaValidator/CardValidator')
const CardRepository = require('./../Repositories/CardRepository')

module.exports.handler = async ({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)
        await JoiValidation(createSchema, body)

        const lastedNumber = body.card.number.substring(10, 16)

        const founded = await CardRepository.findOne({
            ownerId: await CardRepository.convertToObjectId(body.ownerId)
            , platformKey
            , lastedNumber
        })
        
        if (founded) throw new UnprocessableEntityException('Card already exists')

        body.platformKey = platformKey
        body.token = jwt.encode(body.card, platformKey)
        body.lastedNumber = lastedNumber

        const { _id: id} = await CardRepository.create(body)
        return new ResponseCreated({ id })
    }
    catch (error) {
        return new ResponseError(error)
    }
}