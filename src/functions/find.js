const { ResponseSuccess, ResponseError } = require('will-core-lib/http')
const CardRepository = require('./../Repositories/CardRepository')

module.exports.handler = async ({ headers, pathParameters: { id } }) => {
    try {
        id = await CardRepository.convertToObjectId(id)
        const platformKey = headers['x-api-key']

        const card = await CardRepository.findOne({
            _id: id
            , platformKey
        })
        return new ResponseSuccess(card)
    }
    catch (error) {
        return new ResponseError(error)
    }
}