const { ResponseSuccess, ResponseError, ResponseNotContent } = require('will-core-lib/http')
const CardRepository = require('./../Repositories/CardRepository')

module.exports.handler = async ({ queryStringParameters: params, headers }) => {
    try {
        const platformKey = headers['x-api-key']

        params.query = JSON.parse(params.query)
        params.query.platformKey = platformKey

        const cards = await CardRepository.find(params)
        if (!cards.length) return new ResponseNotContent()
        
        return new ResponseSuccess(cards)
    }
    catch (error) {
        return new ResponseError(error)
    }
}