
const mongoose = require('mongoose')

module.exports.CardSchema = new mongoose.Schema({
    ownerId: { type: mongoose.Types.ObjectId, require: true}
    , ownerName: { type: String }
    , lastedNumber: { type: String }
    , token: { type: String, select: false }

    , platformKey: { type: String, select: false }
    // Required in all schemas 
    , createdAt: { type: Date, default: Date.now }
    , updatedAt: { type: Date, default: null }
    , deletedAt: { type: Date, default: null }
})
